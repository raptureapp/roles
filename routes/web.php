<?php

Route::middleware(['web', 'auth'])
    ->namespace('Rapture\Roles\Controllers')
    ->prefix('dashboard')
    ->name('dashboard.')
    ->group(function () {
        Route::resource('roles', 'RolesController')->except('create', 'show', 'destroy');
    });
