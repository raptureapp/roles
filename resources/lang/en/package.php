<?php

return [
    'singular' => 'Role',
    'plural' => 'Roles',
    'super_admin' => 'Super admin',
    'role_name' => 'Role name',
    'none' => 'No Access',
];
