<x-form-section title="Assign Roles" desc="Multiple roles may be applied." :wrap="false">
    <div class="space-y-2">
        @foreach ($roles as $role)
            <x-panel>
                <x-slot:heading class="flex items-center justify-between">
                    <div class="flex-1 cursor-pointer">
                        <p class="font-medium text-gray-900 dark:text-slate-200 cursor-pointer">{{ $role->name }}</p>
                        <p class="text-sm text-gray-500 dark:text-slate-400 mt-1">
                            @if ($role->isSuper())
                                All Permissions
                            @else
                                {{ count($role->permissions) }} Permission{{ count($role->permissions) != 1 ? 's' : '' }}
                            @endif
                        </p>
                    </div>
                    <div class="flex-shrink-0">
                        <x-toggle name="roles[]" value="{{ $role->id }}" :state="$userRoles->contains($role->id)" />
                    </div>
                </x-slot:heading>

                <div class="xl:grid xl:grid-cols-3 xl:gap-x-6 xl:gap-y-4 text-sm">
                    @if ($role->isSuper())
                        <div class="xl:col-span-3">
                            <p class="text-gray-500 dark:text-gray-300">All permissions are granted to this role.</p>
                        </div>
                    @endif
                    @foreach ($role->permissions->groupBy('group') as $group => $permissions)
                        <div class="xl:col-span-1">
                            <strong>{{ $group }}</strong>
                            <ul class="space-y-2 mt-2">
                                @foreach ($permissions as $permission)
                                <li>{{ $permission->keyname }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endforeach
                </div>
            </x-panel>
        @endforeach
    </div>
</x-form-section>
