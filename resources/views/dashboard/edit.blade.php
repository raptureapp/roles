<x-dashboard :title="langAction('edit', __('roles::package.singular'))">
    <x-heading class="flex justify-between items-center">
        <x-h1>@langAction('edit', __('roles::package.singular'))</x-h1>

        <a href="{{ route('dashboard.roles.index') }}">
            <em class="far fa-reply mr-2"></em> @lang('rapture::actions.return')
        </a>
    </x-heading>

    <x-container>
        <x-form method="put" action="{{ route('dashboard.roles.update', $role) }}">
            <div class="space-y-8">
                @hook('roles.edit.before', compact('role'))

                <x-form-section title="Role Information" desc="Enabling super admin will ignore permission selection.">
                    <x-input name="name" :value="$role->name" :label="__('rapture::field.name')" />
                    <x-toggle name="superAdmin" label="Enable Super Admin" :checked="$role->isSuper()" class="mt-6" />
                </x-form-section>

                @hook('roles.edit.between', compact('role'))

                <x-form-section title="Permissions" desc="Click on a group to expand and pick individual permissions." :wrap="false">
                    <div class="space-y-1">
                        @foreach ($groups as $name => $permissions)
                            <x-panel x-data="permissionSelector($el)">
                                <x-slot:heading class="flex justify-between">
                                    <div class="uppercase font-medium dark:text-slate-200">{{ $name }}</div>
                                    <span role="checkbox" tabindex="0" @click.stop="selectAll()" :class="{ 'bg-yellow-400 dark:bg-yellow-700': state === 'partial', 'bg-gray-200 dark:bg-slate-700': state === 'none', 'bg-primary-600 dark:bg-primary-800': state === 'all' }" class="relative inline-flex flex-shrink-0 h-6 w-12 py-1 px-1 rounded-md cursor-pointer transition-colors ease-in-out focus:outline-none focus:ring">
                                        <span aria-hidden="true" :class="{ 'translate-x-3': state === 'partial', 'translate-x-6': state === 'all', 'translate-x-0': state === 'none' }" class="inline-block h-full w-4 rounded bg-white shadow transform transition ease-in-out"></span>
                                    </span>
                                </x-slot:heading>
                                <div class="space-y-3">
                                    @foreach ($permissions as $index => $permission)
                                    <div class="relative flex items-start">
                                        <x-checkbox name="permission[]" id="permission-{{ $permission->id }}" :value="$permission->id" @input="singleSelect()" :checked="in_array($permission->id, $existing)" class="mt-px" />
                                        <label for="permission-{{ $permission->id }}" class="ml-3 text-sm leading-5 cursor-pointer">
                                            <p class="font-medium text-gray-700 dark:text-slate-200">@lang($permission->description)</p>
                                            <p class="text-gray-500 dark:text-slate-400">{{ $permission->keyname }}</p>
                                        </label>
                                    </div>
                                    @endforeach
                                </div>
                            </x-panel>
                        @endforeach
                    </div>
                </x-form-section>

                @hook('roles.edit.after', compact('role'))

                <div class="text-right">
                    <x-button type="submit" size="large" color="primary">
                        @lang('rapture::actions.save')
                    </x-button>
                </div>
            </div>
        </x-form>
    </x-container>

    @push('scripts')
    <script type="text/javascript">
        function permissionSelector(container) {
            const data = {
                open: false,
                state: 'none',
                total: 0,
                selected: 0,
                selectAll() {
                    const newState = this.state !== 'all';

                    Array.from(container.querySelectorAll('[name="permission[]"]')).forEach(function (input) {
                        input.checked = newState;
                    });

                    this.singleSelect();
                },
                singleSelect() {
                    this.selected = container.querySelectorAll('[name="permission[]"]:checked').length;

                    if (this.selected === 0) {
                        this.state = 'none';
                    } else if (this.selected === this.total) {
                        this.state = 'all';
                    } else {
                        this.state = 'partial';
                    }
                },
            };

            data.selected = container.querySelectorAll('[name="permission[]"]:checked').length;
            data.total = container.querySelectorAll('[name="permission[]"]').length;
            data.singleSelect();

            return data;
        }
    </script>
    @endpush
</x-dashboard>
