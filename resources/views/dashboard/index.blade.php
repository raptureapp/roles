<x-dashboard :title="__('roles::package.plural')">
    <x-heading class="flex justify-between items-center" :label="__('roles::package.plural')">
        @can('roles.create', 'roles')
            <x-quick-create :action="route('dashboard.roles.store')" :placeholder="__('roles::package.role_name')" icon="plus">
                <span>@langAction('new', __('roles::package.singular'))</span>
            </x-quick-create>
        @endcan
    </x-heading>

    <livewire:role-table />
</x-dashboard>
