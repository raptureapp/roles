# Roles

Add permission based access to your dashboard

## Installation

Install the package from composer.

```
composer install rapture/roles
```

Publish keeper's config file

```
artisan vendor:publish --tag=config --provider="Rapture\Keeper\KeeperServiceProvider"
```

Inside config/keeper.php change the driver to `roles`.

Import the HasRoles trait to your user model (app/User.php).

```
use Rapture\Roles\Traits\HasRoles;
```

```
use HasRoles;
```

Install the package

```
php artisan package:install rapture/roles
```

## Usage

This package uses Laravel's Gate mechanic so you may restrict access to areas using `@can`
