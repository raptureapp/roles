<?php

namespace Rapture\Roles\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Rapture\Hooks\Facades\Filter;

class StoreRole extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('roles.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Filter::dispatch('roles.store.validation', [
            'name' => 'required|max:255',
        ]);
    }
}
