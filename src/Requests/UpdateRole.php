<?php

namespace Rapture\Roles\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Rapture\Hooks\Facades\Filter;

class UpdateRole extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('roles.edit');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Filter::dispatch('roles.update.validation', [
            'name' => 'required|max:255',
        ]);
    }
}
