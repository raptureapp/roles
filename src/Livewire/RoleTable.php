<?php

namespace Rapture\Roles\Livewire;

use Illuminate\Support\Facades\DB;
use Rapture\Core\Columns\Date;
use Rapture\Core\Columns\ID;
use Rapture\Core\Columns\Text;
use Rapture\Core\Columns\TrueFalse;
use Rapture\Core\Livewire\DatatableComponent;
use Rapture\Core\Table\Action;
use Rapture\Core\Table\Operation;
use Rapture\Hooks\Facades\Hook;
use Rapture\Roles\Events\RoleDeleted;
use Rapture\Roles\Models\Role;

class RoleTable extends DatatableComponent
{
    public $table = 'dashboard.roles';

    public $searchable = true;

    public function columns()
    {
        return [
            ID::make(),
            Text::make('name', __('rapture::field.name'))
                ->visible(),
            TrueFalse::make('super', __('roles::package.super_admin'))
                ->visible(),
            Text::make('permissions_count', 'Permission Count')
                ->filter('numeric')
                ->render(function ($role) {
                    return $role->isSuper() ? 'All' : $role->permissions_count;
                })
                ->query(function ($query) {
                    $query->withCount('permissions');
                }),
            Date::make('created_at', __('rapture::field.created'))
                ->defaultSort(),
        ];
    }

    public function actions()
    {
        return [
            Action::edit('roles')->primary(),
            Action::delete('roles'),
        ];
    }

    public function operations()
    {
        return [
            Operation::make('Mass Delete')
                ->callback('massDelete')
                ->permission('roles.destroy')
                ->render('<em class="far fa-trash"></em>'),
        ];
    }

    public function delete(Role $role)
    {
        $this->authorize('roles.destroy', $role);

        $role->users()->detach();
        $role->delete();

        Hook::dispatch('role.deleted', new RoleDeleted($role));
    }

    public function massDelete()
    {
        Role::whereIn('id', $this->selected)->delete();

        DB::table('role_user')->whereIn('role_id', $this->selected)->delete();

        $this->clearSelection();
    }

    public function query()
    {
        return Role::query();
    }
}
