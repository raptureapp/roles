<?php

namespace Rapture\Roles\Policies;

use Rapture\Keeper\Policies\BasePolicy;

class RolePolicy extends BasePolicy
{
    protected $key = 'roles';
}
