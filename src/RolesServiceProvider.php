<?php

namespace Rapture\Roles;

use Illuminate\Support\Facades\Gate;
use Livewire\Livewire;
use Rapture\Hooks\Facades\Filter;
use Rapture\Hooks\Facades\Hook;
use Rapture\Keeper\Facades\Keeper;
use Rapture\Packages\Package;
use Rapture\Packages\Providers\PackageProvider;
use Rapture\Roles\Drivers\RoleBasedPermission;
use Rapture\Roles\Livewire\RoleTable;
use Rapture\Roles\Models\Role;
use Rapture\Roles\Policies\RolePolicy;

class RolesServiceProvider extends PackageProvider
{
    public function configure(Package $package)
    {
        $package->name('roles')
            ->description('Assign multiple roles per user')
            ->translations()
            ->migrations()
            ->routes('web')
            ->views()
            ->installer();

        Keeper::extend('roles', function () {
            return new RoleBasedPermission();
        });
    }

    public function installed()
    {
        Gate::policy(Role::class, RolePolicy::class);

        Livewire::component('role-table', RoleTable::class);

        Hook::attach('users.create.after', 'Rapture\Roles\Listeners\CreateUserRoleSelection');
        Hook::attach('users.edit.after', 'Rapture\Roles\Listeners\EditUserRoleSelection');

        Hook::attach('user.stored', 'Rapture\Roles\Listeners\SyncRoles');
        Hook::attach('user.updated', 'Rapture\Roles\Listeners\SyncRoles');
        Hook::attach('user.deleted', 'Rapture\Roles\Listeners\DetachRoles');

        Filter::attach('dashboard.users.columns', 'Rapture\Roles\Listeners\UserDirectoryRoleAssignment');
        Filter::attach('users.store.validation', 'Rapture\Roles\Listeners\RoleValidation');
        Filter::attach('users.update.validation', 'Rapture\Roles\Listeners\RoleValidation');
    }
}
