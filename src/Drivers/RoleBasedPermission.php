<?php

namespace Rapture\Roles\Drivers;

use Rapture\Keeper\Contracts\Permissible;

class RoleBasedPermission implements Permissible
{
    /**
     * Resolve a permission check
     *
     * @param  object  $user
     * @param  string  $permission
     * @return bool
     */
    public function allows($user, $permission)
    {
        return $user->hasPermission($permission);
    }
}
