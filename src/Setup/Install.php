<?php

namespace Rapture\Roles\Setup;

use App\Models\User;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Rapture\Packages\BaseInstall;
use Rapture\Roles\Models\Role;

class Install extends BaseInstall
{
    public function handle()
    {
        $this->resourcePermissions('roles');

        $this->addSubMenu([
            'label' => 'roles::package.plural',
            'route' => 'dashboard.roles.index',
            'namespaces' => ['dashboard/roles'],
            'permission' => 'roles.index',
        ], 'dashboard.users.index');

        $exists = Role::first();

        if (!is_null($exists)) {
            return;
        }

        $role = Role::create([
            'name' => 'Super Admin',
            'super' => true,
        ]);

        $allUsers = User::all();

        foreach ($allUsers as $user) {
            DB::table('role_user')->insert([
                'user_id' => $user->id,
                'role_id' => $role->id,
            ]);
        }
    }

    public function prepareFiles()
    {
        $this->userModel();
        $this->keeperConfig();
    }

    public function userModel()
    {
        $modified = false;
        $userPath = base_path(str(config('auth.providers.users.model'))->replace('\\', '/')->replace('App', 'app') . '.php');
        $userModel = str(file_get_contents($userPath));

        if (!$userModel->contains('HasRoles')) {
            $traits = $userModel->afterLast('use ')->before(';');
            $userModel = $userModel->replace($traits, $traits . ', HasRoles');

            $useStatements = $userModel->betweenFirst('use ', 'class User')->beforeLast(';') . ';';
            $userModel = $userModel->replace($useStatements, str($useStatements)->newLine()->append('use Rapture\Roles\Traits\HasRoles;'));
            $modified = true;
        }

        if ($modified) {
            file_put_contents($userPath, $userModel);
        }
    }

    public function keeperConfig()
    {
        Artisan::call('vendor:publish', [
            '--tag' => 'config',
            '--provider' => 'Rapture\Keeper\KeeperServiceProvider',
        ]);

        $modified = false;
        $path = config_path('keeper.php');
        $file = str(file_get_contents($path));

        if ($file->contains('always')) {
            $file = $file->replace("'driver' => 'always'", "'driver' => 'roles'");
            $modified = true;
        }

        if ($modified) {
            file_put_contents($path, $file);
        }
    }
}
