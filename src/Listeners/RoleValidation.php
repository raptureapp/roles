<?php

namespace Rapture\Roles\Listeners;

class RoleValidation
{
    public function handle($rules)
    {
        $rules['roles'] = 'required';

        return $rules;
    }
}
