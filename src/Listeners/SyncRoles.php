<?php

namespace Rapture\Roles\Listeners;

class SyncRoles
{
    public function handle($event)
    {
        $event->user->roles()->sync(request()->input('roles'));
    }
}
