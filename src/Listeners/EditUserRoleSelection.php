<?php

namespace Rapture\Roles\Listeners;

use Rapture\Roles\Models\Role;

class EditUserRoleSelection
{
    public function handle($data)
    {
        $roles = Role::with('permissions')->get();

        echo view('roles::users.edit', [
            'roles' => $roles,
            'userRoles' => $data['user']->roles->pluck('id'),
        ]);
    }
}
