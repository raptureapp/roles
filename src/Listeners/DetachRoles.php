<?php

namespace Rapture\Roles\Listeners;

class DetachRoles
{
    public function handle($event)
    {
        $event->user->roles()->detach();
    }
}
