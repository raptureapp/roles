<?php

namespace Rapture\Roles\Listeners;

use Rapture\Core\Columns\Relationship;
use Rapture\Roles\Models\Role;

class UserDirectoryRoleAssignment
{
    public function handle($columns)
    {
        $columns[] = Relationship::make('role', __('roles::package.singular'))
            ->hasMany(Role::class, 'roles', 'name')
            ->visible();

        return $columns;
    }
}
