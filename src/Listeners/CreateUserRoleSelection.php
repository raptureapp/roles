<?php

namespace Rapture\Roles\Listeners;

use Rapture\Roles\Models\Role;

class CreateUserRoleSelection
{
    public function handle()
    {
        $roles = Role::with('permissions')->get();

        echo view('roles::users.create', compact('roles'));
    }
}
