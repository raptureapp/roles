<?php

namespace Rapture\Roles\Traits;

trait HasRoles
{
    public function roles()
    {
        return $this->belongsToMany('Rapture\Roles\Models\Role');
    }

    public function isSuper()
    {
        return $this->roles->filter(function ($role) {
            return $role->isSuper();
        })->count() > 0;
    }

    public function hasPermission($keyname)
    {
        if ($this->isSuper()) {
            return true;
        }

        return $this->roles->filter(function ($role) use ($keyname) {
            return $role->hasPermission($keyname);
        })->count() > 0;
    }
}
