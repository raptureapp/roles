<?php

namespace Rapture\Roles\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Rapture\Hooks\Facades\Hook;
use Rapture\Keeper\Models\Permission;
use Rapture\Roles\Events\RoleCreated;
use Rapture\Roles\Events\RoleUpdated;
use Rapture\Roles\Models\Role;
use Rapture\Roles\Requests\StoreRole;
use Rapture\Roles\Requests\UpdateRole;

class RolesController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Role::class, 'role');
    }

    public function index()
    {
        return view('roles::dashboard.index');
    }

    public function store(StoreRole $request)
    {
        $role = Role::create([
            'name' => $request->input('name'),
        ]);

        Hook::dispatch('role.stored', new RoleCreated($role));

        return redirect()->route('dashboard.roles.edit', $role->id);
    }

    public function edit(Role $role)
    {
        $permissions = Permission::where('active', 1)->orderBy('priority')->get();

        $groups = $permissions->mapToGroups(function ($item) {
            return [$item->group => $item];
        })->sortKeys();

        return view('roles::dashboard.edit', [
            'role' => $role,
            'groups' => $groups,
            'existing' => $role->getPermissionKeys()->toArray(),
        ]);
    }

    public function update(UpdateRole $request, Role $role)
    {
        $role->name = $request->input('name');
        $role->super = $request->input('superAdmin') === 'true';
        $role->save();

        $role->permissions()->sync($request->input('permission', []));

        Hook::dispatch('role.updated', new RoleUpdated($role));

        return redirect()
            ->route('dashboard.roles.index')
            ->with('status', langAlert('updated', __('roles::package.singular')));
    }
}
