<?php

namespace Rapture\Roles\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Role extends Model
{
    use Searchable;

    protected $guarded = [];

    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }

    public function permissions()
    {
        return $this->belongsToMany('Rapture\Keeper\Models\Permission');
    }

    public function toSearchableArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }

    public function isSuper()
    {
        return $this->super;
    }

    public function getPermissionKeys()
    {
        return $this->permissions->pluck('id');
    }

    public function getPermissionsNames()
    {
        return $this->permissions->pluck('keyname');
    }

    public function hasPermission($keyname)
    {
        return $this->getPermissionsNames()->contains($keyname);
    }
}
